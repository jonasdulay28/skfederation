//Vue
import Vue from 'vue'
//router
import VueRouter from 'vue-router'
//components
import Admin_Dashboard from './Admin_Dashboard.vue'
import Suggestions from 'v-suggestions'
//import 'v-suggestions/dist/v-suggestions.css' // you can import the stylesheets also (optional)

window.Vue = Vue;
//axios
import VueAxios from 'vue-axios'
import axios from 'axios'
import Dashboard from './admin/Dashboard.vue'

import View_Users from './admin/Users/View.vue'
import Add_Users from './admin/Users/Add.vue'
import Edit_Users from './admin/Users/Edit.vue'

import View_Records from './admin/Records/View.vue'
import Add_Record from './admin/Records/Add.vue'
import Edit_Record from './admin/Records/Edit.vue'



import Navbar from './admin/Header.vue'
import Footer from './admin/Footer.vue'
import Sidebar from './admin/Sidebar.vue'
import VueCkeditor from 'vue-ckeditor2';
import DatatableFactory from 'vuejs-datatable';


Vue.use(VueCkeditor);

window.axios = axios
Vue.use(VueAxios, axios)
Vue.use(VueRouter);
Vue.use(Suggestions);
Vue.use(DatatableFactory);
Vue.component('vue-ckeditor',VueCkeditor);
Vue.component('admin_header', Navbar);
Vue.component('admin_footer', Footer);
Vue.component('admin_sidebar', Sidebar);
/*export const resumeBus = new Vue();*/

const routes = [
	{
		name: '/',
		path: '/',
		component: Dashboard
	},
	{
		name: '/view_users',
		path: '/view_users',
		component: View_Users
	},
	{
		name: '/add_users',
		path: '/add_users',
		component: Add_Users
	},
	{
		name: '/edit_user',
		path: '/edit_user/:id',
		component: Edit_Users
	},
	{
		name: '/view_records',
		path: '/view_records',
		component: View_Records
	},
	{
		name: '/add_record',
		path: '/add_record',
		component: Add_Record
	},
	{
		name: '/edit_record',
		path: '/edit_record/:id',
		component: Edit_Record
	}
];
const router = new VueRouter({
	routes,
	linkExactActiveClass: "active" // active class for *exact* links.,
	//,mode: 'history'
});


new Vue({
    el: '#app',
    router,
    render: app => app(Admin_Dashboard)
});