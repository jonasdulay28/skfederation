<!DOCTYPE html>
<html class="apply_page">
   <head>
      <!-- Site made with Mobirise Website Builder v4.9.2, https://mobirise.com -->
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="generator" content="Mobirise v4.9.2, mobirise.com">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
      <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
      <meta name="description" content="">
      <title>Letran Admission</title>
      <link rel="stylesheet" href="<?php echo base_url()?>assets/web/assets/mobirise-icons/mobirise-icons.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/tether/tether.min.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-grid.min.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-reboot.min.css">
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
      <style type="text/css">
         input{
            text-transform: uppercase;
         }
      </style>
   </head>
   <body >
      <div class="container-fluid" style="background: #fff;">
         <div class="row">
            <div class="col-md-12">
               <img src="<?php echo images_bundle()?>logo.png" style="height: 80px;margin:0 auto;padding:10px;">
            </div>
         </div>
              
      </div>
      <div class="hero-image">
        <!-- <div class="hero-text">
          <h1>I am John Doe</h1>
          <p>And I'm a Photographer</p>
          <button>Hire me</button>
        </div> -->
      </div>
      <div class="container" style="margin-top:50px;">
         <div class="row">
            <div class="col-md-12">
               <div class="page-header">
                  <center><h1 class="text-primary">Online Student Application<br /></h1></center>
               </div>
            </div>
            <div class="col-md-12">
               <?php echo form_open_multipart("Admission/apply",array('method'=>'POST','autocomplete'=>'off','id'=>'application_form')); ?>
                  <div class="validation-summary-valid" data-valmsg-summary="true">
                     <ul>
                        <li style="display:none"></li>
                     </ul>
                  </div>
                  <div class="form-group">
                     <!-- <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <input id="TypeOfApplication" name="TypeOfApplication" type="hidden" value="" />
                              <span class="field-validation-valid text-danger" data-valmsg-for="TypeOfApplication" data-valmsg-replace="true"></span>
                              <h4 class="text-primary">Basic Education</h4>
                           </div>
                           <div class="form-group">
                              <label for="YearEntry">School Year</label>
                              <select class="form-control" data-val="true" data-val-number="The field Entry Year must be a number." data-val-required="The Entry Year field is required." id="YearEntry" name="YearEntry">
                                 <option value="">Select year of entry</option>
                                 <option value="343">Regular Period, 2019-2020</option>
                              </select>
                              <span class="field-validation-valid text-danger" data-valmsg-for="YearEntry" data-valmsg-replace="true"></span>
                           </div>
                           <div class="form-group">
                              <div class="col-md-12">
                                 <input data-val="true" data-val-number="The field EducationLevel must be a number." id="EducationLevel" name="EducationLevel" type="hidden" value="" />
                              </div>
                           </div>
                           <div class="form-group">
                              <label>Grade Level</label>
                              <select class="form-control" data-val="true" data-val-required="The GradeLevel field is required." id="GradeLevel" name="GradeLevel">
                                 <option value="">Select grade level</option>
                                 <option value="7">GRADE 7</option>
                              </select>
                              <span class="field-validation-valid text-danger" data-valmsg-for="GradeLevel" data-valmsg-replace="true"></span>
                           </div>
                        </div>
                     </div> -->
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">Personal Information</h4>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label for="LastName">Last Name</label>
                              <input class="form-control" data-val="true" id="last_name" name="last_name" type="text" value="" required  />
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label for="FirstName">Given Name</label>
                              <input class="form-control" id="first_name" name="first_name" type="text" value="" required />
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label for="MiddleName">Middle Name</label>
                              <input class="form-control"id="middle_name" name="middle_name" type="text" value=""  />
                           </div>
                        </div>
                        <div class="col-md-1">
                           <div class="form-group">
                              <label for="MiddleName">Suffix</label>
                              <input class="form-control" type="text" value="" name="suffix" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="Address">House No./Street</label>
                              <input class="form-control"  id="address" name="address" type="text" value="" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="Address">Subdivision</label>
                              <input class="form-control" id="subdivision" name="subdivision" type="text" value="" />
                           </div>
                        </div>
                        <div class="col-md-5">
                           <div class="form-group">
                              <label for="Province">Barangay/Town</label>
                              <input class="form-control" type="text" name="town" value="" />
                           </div>
                        </div>
                        <div class="col-md-5">
                           <div class="form-group">
                              <label for="City">Province/City</label>
                              <input class="form-control" type="text" name="city" value="" />
                           </div>
                        </div>
                        <div class="col-md-2">
                           <div class="form-group">
                              <label for="City">Postal Code</label>
                              <input class="form-control" type="text" name="postal_code" value="" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="Birthday">Birthday</label>
                              <input class="form-control" id="Birthday" name="birthday" type="date" value="" required />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="BirthPlace">Birth Place</label>
                              <input class="form-control" id="BirthPlace" name="birth_place" type="text" value="" required />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="LandlineNo">Landline No</label>
                              <input class="form-control" id="LandlineNo" name="landline" type="text" value="" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="MobileNo">Mobile No</label>
                              <input class="form-control" id="MobileNo" name="mobile" type="text" value="" />
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label for="Citizenship">Citizenship</label>
                              <input class="form-control" id="Citizenship" name="citizenship" type="text" value="" required />
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label for="CivilStatus">Civil Status</label>
                              <select class="form-control" id="statustype" name="civil_status" >
                                 <option value="SINGLE">Single</option>
                                 <option value="SEPERATED">Seperated</option>
                                 <option value="MARRIED">Married</option>
                                 <option value="DIVORCED">Divorced</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label for="Religion">Religion</label>
                              <input class="form-control" id="Religion" name="religion" type="text" value="" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="EmailAddress">Email Address</label>
                              <input class="form-control"  id="EmailAddress" name="email" type="text" value="" required/>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="Gender">Gender</label>
                              <select class="form-control" id="Gender" name="gender">
                                 <option value="M">Male</option>
                                 <option value="F">Female</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-6">
                           <div class="form-group">
                              <h4 class="text-primary">Father's Information</h4>
                           </div>
                           <div class="form-group">
                              <label for="FatherName">Father&#39;s Name</label>
                              <input class="form-control"  id="FatherName" name="father_name" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label for="FatherContact">Father&#39;s Contact No</label>
                              <input class="form-control"  id="FatherContact" name="father_contact" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label for="FatherEmail">Father&#39;s Email</label>
                              <input class="form-control" id="FatherEmail" name="father_email" type="text" value="" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <h4 class="text-primary">Mother's Information</h4>
                           </div>
                           <div class="form-group">
                              <label for="MotherName">Mother&#39;s Name</label>
                              <input class="form-control" id="MotherName" name="mother_name" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label for="MotherContact">Mother&#39;s Contact No</label>
                              <input class="form-control" id="MotherContact" name="mother_contact" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label for="MotherEmail">Mother&#39;s Email</label>
                              <input class="form-control " id="MotherEmail" name="mother_email" type="text" value="" />
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <label for="MotherName">Number of Siblings</label>
                              <input class="form-control" data-val="true" id="MotherName" name="no_sibling" type="text" value="" />
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">Guardian's Information</h4>
                           </div>
                           <div class="form-group">
                              <label for="GuardianName">Guardian&#39;s Name</label>
                              <input class="form-control" id="GuardianName" name="guardian_name" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label for="GuardianAddress">Guardian&#39;s Address</label>
                              <input class="form-control"id="GuardianAddress" name="guardian_address" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label>Relationship to Guardian</label>
                              <input class="form-control" id="RelationshipToGuardian" name="guardian_relationship" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label for="GuardianContactNo">Guardian Contact No</label>
                              <input class="form-control" id="GuardianContactNo" name="guardian_contact" type="text" value="" />
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">Educational Data</h4>
                           </div>
                           <div class="form-group">
                              <label for="FormerSchool">School Last Attended</label>
                              <input class="form-control"  id="FormerSchool" name="former_school" type="text" value="" />
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                              <div class="form-group">
                                 <label for="Former_School_Type">Program (Transferee only):</label>
                                 <input class="form-control" id="FormerSchool" name="program" type="text" value="" />
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="Former_School_Type">School Year</label>
                                 <input class="form-control"id="FormerSchool" name="school_year" type="text" value="" />
                              </div>
                           </div>
                           </div>
                           <div class="form-group">
                              <label for="FormerSchoolAddress">School Address</label>
                              <input class="form-control"  id="FormerSchoolAddress" name="school_address" type="text" value="" />
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label for="GPA">General Weighted Average 1st sem</label>
                                    <input class="form-control"  id="GPA" name="gwa_first" type="text" value="" />
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label for="GPA">General Weighted Average 2nd sem</label>
                                    <input class="form-control" id="GPA" name="gwa_second" type="text" value="" />
                                 </div>
                              </div>
                           </div>
                           <label for="GPA">Senior High School Track (To be completed by college freshmen applicant):</label>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <select class="form-control" name="track_type">
                                       <option value="ACADEMIC">Academic</option>
                                       <option value="ARTS AND DESIGN">Arts and Design</option>
                                       <option value="SPORTS">Sports</option>
                                       <option value="TECHNOLOGY-VOCATIONAL-LIVELIHOOD">Technology-Vocational-Livelihood</option>
                                    </select>   
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <select class="form-control" name="track_sub_type">
                                       <option value="STEM">STEM</option>
                                       <option value="ABM">ABM</option>
                                       <option value="HUMSS">HUMSS</option>
                                       <option value="GA">GA</option>
                                       <option value="HOME ECONOMICS">Home Economics</option>
                                       <option value="AGRI-FISHERY">Agri-Fishery</option>
                                       <option value="INDUSTRIAL ARTS">Industrial Arts</option>
                                       <option value="INFORMATION AND COMMUNICATIONS TECHNOLOGY">Information and Communications Technology</option>
                                    </select>   
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="HonorsReceived">Honors Received</label>
                              <input class="form-control" id="HonorsReceived" name="honors" type="text" value="" />
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">Reasons for applying at Letran Calamba?</h4>
                           </div>
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="QUALITY EDUCATION" />Quality Education</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="AFFORDABILITY" />Affordability</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="GOOD FACILITIES" />Good Facilities</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="PROXIMITY" />Proximity</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="AVAILABILITY OF PROGRAM" />Availability of Program</label>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="RECOMMENDED BY SCHOOL" />Recommended by School</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="COMPETENT PROFESSORS" />Competent Professors</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="CAMPUS ENVIRONMENT" />Campus Environment</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="ALUMNUS" />Alumnus</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="checkbox">
                                 <label>Others</label>
                              </div>
                              <input class="form-control" id="Q7" name="apply_others" style="font-size:16px;" type="text" value="" />                            
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">How did you come to know about Letran Calamba? Through</h4>
                           </div>
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="STUDENTS OF LETRAN" />Students of Letran</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="EMPLOYEES OF LETRAN" />Employees of Letran</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="FACULTY OF LETRAN" />Faculty of Letran</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="NEWSPAPER ADS" />Newspaper Ads</label>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                     <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="BILLBOARDS" />Billboards</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="STREAMERS" />Streamers</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="CAREER ORIENTATION / TALK" />Career Orientation / Talk</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="checkbox">
                                 <label>Others</label>
                              </div>
                              <input class="form-control" id="Q7" name="referal_others" style="font-size:16px;" type="text" value="" />                            
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">Documents (optional)</h4>
                           </div>
                           <div class="form-group">
                              <label for="FormerSchoolAddress">1x1 recent ID picture</label><br>
                              <input   id="" name="id_picture" type="file" value="" accept="image/*,application/pdf" required />
                           </div>
                           <div class="form-group">
                              <label for="FormerSchoolAddress">Certificate of Candidacy for Promotion of Graduation</label><br>
                              <input  id="" name="certificate_of_candidacy" type="file" value="" accept="image/*,application/pdf" required />
                           </div>
                           <div class="form-group">
                              <label for="FormerSchoolAddress">Photocopy of NSO/PSA Authenticated Birth Certificate</label><br>
                              <input id="" name="nso_psa" type="file" value="" accept="image/*,application/pdf"  required/>
                           </div>
                           <div class="form-group">
                              <label for="FormerSchoolAddress">Photocopy of Report Card (Form 138)</label><br>
                              <input id="" name="report_card" type="file" value=""  accept="image/*,application/pdf"  required />
                           </div>
                           <div class="form-group">
                              <label for="FormerSchoolAddress">Photocopy Certified True Copy of Grades</label><br>
                              <input id="" name="copy_of_grade" type="file" value="" accept="image/*,application/pdf" required />
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="form-group">
                                 <h4 class="text-primary">Certification</h4>
                              </div>
                           </div>
                           <div class="form-group">
                              <p>
                                 <input data-val="true"  name="Certification" type="checkbox" value="true" required />
                                 I hereby certify that all information indicated herein and all supporting documents are true
                                 and correct. I fully understand that any misrepresentation or failure to disclose on my part as requiress herein, may cause the disapproval of this application. In the event that the
                                 application is approved, it is deemed that I shall accept and abide by the policies, procedures, and conditions set by Colegio de San Juan de Letran.I hereby acknowledge that the entrance examination, regardless of the result, is not a guarantee for admission. The Colegio has the right to verify the records submitted by the student/guardian. I am willing to abide to the rules and regulations of the Colegio pertaining to admission.
                              </p>
                              <input type="submit" style="letter-spacing:4px; border-radius:0px; text-align:center; width:150px; border-width:2px; border-color:white; color:white;  font-size:14px;" value="SUBMIT" class="btn btn-success " />
                           </div>
                        </div>
                     </div>
                  </div>
               <?php echo form_close() ?>
            </div>
         </div>
      </div>
      <!--footer starts from here-->
      <footer class="footer">
      <div class="container bottom_border">
      <div class="row">
      <div class=" col-sm-4 col-md col-sm-4  col-12 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Find us</h5>
      <!--headin5_amrc-->
      <p class="mb10">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
      <p><i class="fa fa-location-arrow"></i> 9878/25 sec 9 rohini 35 </p>
      <p><i class="fa fa-phone"></i>  +91-9999878398  </p>
      <p><i class="fa fa fa-envelope"></i> info@example.com  </p>


      </div>


      <div class=" col-sm-4 col-md  col-6 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Quick links</h5>
      <!--headin5_amrc-->
      <ul class="footer_ul_amrc">
      <li><a href="">Home</a></li>
      <li><a href="">About</a></li>
      <li><a href="">Academics</a></li>
      <li><a href="">Admission</a></li>
      <li><a href="">Departmen</a></li>
      <li><a href="">Contact</a></li>
      </ul>
      <!--footer_ul_amrc ends here-->
      </div>


      <div class=" col-sm-4 col-md  col-6 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Quick links</h5>
      <!--headin5_amrc-->
      <ul class="footer_ul_amrc">
      <li><a href="">Home</a></li>
      <li><a href="">About</a></li>
      <li><a href="">Academics</a></li>
      <li><a href="">Admission</a></li>
      <li><a href="">Departmen</a></li>
      <li><a href="">Contact</a></li>
      </ul>
      <!--footer_ul_amrc ends here-->
      </div>


      <div class=" col-sm-4 col-md  col-12 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Follow us</h5>
      <!--headin5_amrc ends here-->

      <ul class="footer_ul2_amrc">
      <li><a href="#"><i class="fab fa-twitter fleft padding-right"></i> </a><p>Lorem Ipsum is simply dummy text of the printing...<a href="#">https://www.lipsum.com/</a></p></li>
      <li><a href="#"><i class="fab fa-twitter fleft padding-right"></i> </a><p>Lorem Ipsum is simply dummy text of the printing...<a href="#">https://www.lipsum.com/</a></p></li>
      <li><a href="#"><i class="fab fa-twitter fleft padding-right"></i> </a><p>Lorem Ipsum is simply dummy text of the printing...<a href="#">https://www.lipsum.com/</a></p></li>
      </ul>
      <!--footer_ul2_amrc ends here-->
      </div>
      </div>
      </div>


      <div class="container">
      <ul class="foote_bottom_ul_amrc">
      <li><a href="">Home</a></li>
      <li><a href="">About</a></li>
      <li><a href="">Academics</a></li>
      <li><a href="">Admission</a></li>
      <li><a href="">Departmen</a></li>
      <li><a href="">Contact</a></li>
      </ul>
      <!--foote_bottom_ul_amrc ends here-->
      <p class="text-center">Copyright @2019 | Copyright <a href="#">Colegio de San Juan Letran Calamba</a></p>
      <!--social_footer_ul ends here-->
      </div>

      </footer>

      <script src="<?php echo base_url()?>assets/web/assets/jquery/jquery.min.js"></script>
      <script src="<?php echo base_url()?>assets/popper/popper.min.js"></script>
      <script src="<?php echo base_url()?>assets/tether/tether.min.js"></script>
      <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url()?>assets/dropdown/js/script.min.js"></script>
      <script src="<?php echo base_url()?>assets/touchswipe/jquery.touch-swipe.min.js"></script>
      <script src="<?php echo base_url()?>assets/smoothscroll/smooth-scroll.js"></script>
      <script src="<?php echo base_url()?>assets/theme/js/script.js"></script>
   </body>
</html>