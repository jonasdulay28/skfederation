<!DOCTYPE html>
<html class="admission_page">
<head>
  <!-- Site made with Mobirise Website Builder v4.9.2, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.9.2, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
  <meta name="description" content="">
  <title>Letran Admission</title>
  <link rel="stylesheet" href="<?php echo base_url()?>assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/tether/tether.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo styles_bundle()?>sweetalert2.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
  
  
</head>
<body class="admission-bg">
	<div  class="container-fluid" style="background: #fff;padding:10px 20px;">
		<div class="row">
			<div class="col-md-12">
				<center>
				<img src="<?php echo images_bundle()?>logo.png" class="img-fluid"  style="height: 70px;">
				</center>
			</div>
		</div>
	</div>
	<div class="container" style="margin-top: 12%;">
	    <div class="row"> 
	        <div class="col-md-5">
	            <h1 class="admission-header" >ONLINE APPLICATION</h1>
	            <a href="<?php echo base_url('Admission/apply')?>"><button class="btn  apply_now" style="width: 100%;margin-bottom: 25px;margin-top: 15px;">Apply Now</button></a>
	            <button class="btn admission_cta" style="width: 100%;margin-bottom: 15px;" data-toggle="modal" data-target="#exampleModalLong">Admission Requirements</button>
	        </div>
	        <div class="col-md-6 offset-md-1 offset-sm-0">
                <?php echo form_open("Authentication/login",array('method'=>'POST','autocomplete'=>'off','id'=>'login_form')); ?>
	            <h1 class="admission-header">USER LOGIN</h1>
	            <label>Reference Number:</label>
	            <input type="text" name="reference_number" class="form-control" placeholder="Reference number" style="margin-bottom: 10px;border-radius: 0px;">
	            <label>Password:</label>
	            <input type="password" name="password" class="form-control" placeholder="Password" style="margin-bottom: 15px;border-radius: 0px;">
	            <center><button class="btn admission_cta">Login</button></center>
                <?php echo form_close() ?>
	        </div>
	    </div>
	</div>
        <footer style="position: absolute;bottom: 0;min-height: 80px;background: #131313">    
           <div class="container">
          <ul class="foote_bottom_ul_amrc">
          <li><a href="">Home</a></li>
          <li><a href="">About</a></li>
          <li><a href="">Academics</a></li>
          <li><a href="">Admission</a></li>
          <li><a href="">Departmen</a></li>
          <li><a href="">Contact</a></li>
          </ul>
          <!--foote_bottom_ul_amrc ends here-->
          <p class="text-center">Copyright @2019 | Copyright <a href="#">Colegio de San Juan Letran Calamba</a></p>
          <!--social_footer_ul ends here-->
          </div>
        </footer>

	<!-- Modal -->
	<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLongTitle">Admission Requirements</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <p id="pcontentwhite">
                            Kinder & Elementary<br /><br />
                            <ul id="pcontentwhite">
                                <li>
                                    FOR KINDER APPLICANTS
                                    <ul>
                                        <li>Two (2) 2” x 2” recent picture with white background</li>

                                        <li>Photocopy of PSA Birth Certificate</li>

                                        <li>Photocopy of Baptismal Certificate (For Catholic applicants only)</li>

                                        <li>Photocopy of Alien Certificate of Registration (For foreign applicants only)</li>
                                    </ul>
                                </li>
                                <li>
                                    FOR GRADE 1 APPLICANTS
                                    <ul>
                                        <li>Two (2) 2” x 2” recent picture with white background</li>

                                        <li>Certified true copy of the most recent kindergarten report card (at least up to 1st grading period).</li>

                                        <li>Photocopy of Latest School I.D. card</li>
                                        <li>Photocopy of PSA Birth Certificate</li>

                                        <li>
                                            Photocopy of Baptismal Certificate (For Catholic applicants only)
                                        </li>

                                        <li> Photocopy of Alien Certificate of Registration (For foreign applicants only)</li>
                                    </ul>
                                </li>
                                <li>
                                    FOR TRANSFEREES (GRADE 2 – 6)
                                    <ul>
                                        <li>Two (2) 2” x 2” recent picture with white background</li>
                                        <li>Certified true copy of the latest report card. (At least up to 2nd grading period)</li>
                                        <li>Photocopy of Latest School I.D. card</li>
                                        <li>Photocopy of PSA Birth Certificate</li>
                                        <li>Photocopy of Baptismal Certificate (For Catholic applicants only)</li>
                                        <li>Photocopy of Alien Certificate of Registration (For foreign applicants only)</li>
                                    </ul>
                                </li>



                            </ul>
                        </p>
                        <p id="pcontentwhite">
                            Junior High School<br /><br />
                            <ul id="pcontentwhite">
                                <li>
                                    FOR GRADE 7 APPLICANTS
                                    <ul>
                                        <li>Two (2) 2” x 2” recent picture with white background</li>

                                        <li>
                                            Certified true copies of the student applicant’s complete 5th Grade report card and the latest 6th Grade report card (at least up to 1st grading period).
                                        </li>

                                        <li>Photocopy of Latest School I.D. card</li>

                                        <li>Photocopy of PSA Birth Certificate</li>

                                        <li>Photocopy of Baptismal Certificate (For Catholic applicants only)</li>

                                        <li>Photocopy of Alien Certificate of Registration (For foreign applicants only)</li>
                                    </ul>
                                </li>
                                <li>
                                    FOR TRANSFEREES (GRADE 8 – 10)
                                    <ul>
                                        <li>Two (2) 2” x 2” recent picture with white background</li>

                                        <li>Certified true copy of the latest report card. (At least up to 2nd grading period)</li>

                                        <li>Photocopy of Latest School I.D. card</li>

                                        <li>Photocopy of PSA Birth Certificate</li>

                                        <li>Photocopy of Baptismal Certificate (For Catholic applicants only)</li>

                                        <li> Photocopy of Alien Certificate of Registration (For foreign applicants only)</li>
                                    </ul>
                                </li>
                            </ul>

                        </p>
                        <p id="pcontentwhite">
                            Senior High School<br /><br />
                            <ul id="pcontentwhite">
                                <li>
                                    FOR NEW STUDENTS
                                    <ul>
                                        <li>Two (2) 2” x 2” recent picture with white background</li>

                                        <li>Certified true copies of the student applicant’s complete 9th Grade report card and the latest 10th Grade report card (at least up to 1st grading period).</li>

                                        <li>Photocopy of Latest School I.D. card</li>

                                        <li>Photocopy of PSA Birth Certificate</li>

                                        <li>Photocopy of Baptismal Certificate (For Catholic applicants only)</li>

                                        <li>Photocopy of Alien Certificate of Registration (For foreign applicants only)</li>

                                        <li>Photocopy of NCAE Result</li>
                                    </ul>
                                </li>
                                <li>
                                    FOR TRANSFEREES
                                    <ul>
                                        <li>Two (2) 2” x 2” recent picture with white background</li>

                                        <li>Certified true copy of the latest report card. (At least up to 2nd grading period)</li>

                                        <li>Photocopy of Latest School I.D. card</li>

                                        <li>Photocopy of PSA Birth Certificate</li>

                                        <li>Photocopy of Baptismal Certificate (For Catholic applicants only)</li>

                                        <li>Photocopy of Alien Certificate of Registration (For foreign applicants only)</li>

                                        <li>Photocopy of NCAE Result</li>
                                    </ul>
                                </li>
                            </ul>

                        </p>
                        <p id="pcontentwhite">
                            Collegiate<br /><br />
                            <ul id="pcontentwhite">
                                <li>
                                    FOR NEW STUDENTS
                                    <ul>
                                        <li>Two (2) 2” x 2” recent picture with white background</li>

                                        <li>Certified true copies of the student applicant’s complete 11th Grade report card and the latest 12th Grade report card (at least up to 1st grading period).</li>

                                        <li>Photocopy of Latest School I.D. card</li>

                                        <li>Photocopy of PSA Birth Certificate</li>

                                        <li>Photocopy of Baptismal Certificate (For Catholic applicants only)</li>

                                        <li>Photocopy of Alien Certificate of Registration (For foreign applicants only)</li>
                                    </ul>
                                </li>
                                <li>
                                    FOR TRANSFEREES
                                    <ul>
                                        <li>Two (2) 2” x 2” recent picture with white background</li>

                                        <li>Certified true copy of grades or Transcript of Records (TOR)</li>

                                        <li>Photocopy of Latest School I.D. card</li>

                                        <li>Photocopy of PSA Birth Certificate</li>

                                        <li> Photocopy of Baptismal Certificate (For Catholic applicants only)</li>

                                        <li>Photocopy of Alien Certificate of Registration (For foreign applicants only)</li>
                                    </ul>
                                </li>
                            </ul>

                        </p>

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <a href="<?php echo base_url('Admission/apply')?>"><button type="button" class="btn btn-primary">Apply Online</button></a>
	      </div>
	    </div>
	  </div>
	</div>

  <script src="<?php echo base_url()?>assets/web/assets/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url()?>assets/popper/popper.min.js"></script>
  <script src="<?php echo base_url()?>assets/tether/tether.min.js"></script>
  <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo scripts_bundle()?>sweetalert2.min.js"></script>
  <script src="<?php echo scripts_bundle()?>global.js"></script>
  <script type="text/javascript">
    var base_url = "<?php echo base_url()?>";
    $("#login_form").on("submit",function(e){
        e.preventDefault();
        var datastring = $("#login_form").serialize();
        $.ajax({
            type: "POST",
            url: base_url+"Authentication/login",
            data: datastring,
            dataType: "json",
            success: function(data) {
                if(data.message == "success") {
                    window.location.href = "Dashboard"
                } else {
                    swal("Error", "Invalid reference number or password", "error")
                }

            },
            error: function(err) {
                console.log(err);
            }
        });
    })
  </script>
  
</body>
</html>