<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Records extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model('Admin_model');
	}

	public function get_records() {
		$data['records'] = $this->Admin_model->fetch('records');
		echo json_encode($data);
	}

	public function add()
	{
		$response = ["message"=>"success"];
		$records_data = json_decode(post('records_data'));
		$this->Admin_model->insert('records',$records_data);
		echo json_encode($response);
	}

	public function get_record(){
		$id = clean_data(rawurldecode(get('q')));
		$filter = ["id"=>$id];
		$data["record"] = $this->Admin_model->fetch_data('records',$filter);
		echo json_encode($data);
	}

	public function get_barangay(){
		$data['brgy'] = $this->Admin_model->fetch('refbrgy');
		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$records_data = json_decode(post('records_data'));
		$id = clean_data(post('id'));
		$filter = ["id"=>$id]; 
		$this->Admin_model->update('records',$records_data,$filter);
		echo json_encode($response);
	}

	public function delete()
	{
		$id = clean_data(post('id'));
		$filter = ["id"=>$id]; 
		$this->Admin_model->delete('records',$filter);
		echo json_encode($response);
	}


	
}