<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Admin_model");

		if(!isset($this->session->ses_id)){
			redirect(base_url('Admission'));
		}
	}

	public function index()
	{
		$this->load->view('templates/dashboard_template');
	}

	public function add()
	{

	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function logout(){
		session_destroy();
		redirect(base_url('Admission'));
	}

	public function add_feedback() {
		$feedback = clean_data(post('feedback'));
		$reference_number = decrypt($this->session->ses_id);
		$data = ["feedback"=>$feedback,"reference_number"=>$reference_number];
		$this->db->insert('feedback',$data);
		echo json_encode(["message"=>"success"]);
	}

	public function get_dashboard_data(){
		$data["num_users"] = $this->Admin_model->count_rows("users");
		$data["num_records"] = $this->Admin_model->count_rows("records");
		echo json_encode($data);
	}

	public function validate_key(){
		$response= ["message"=> "success"];
		$permit_key = clean_data(post('permit_key'));
		$reference_number = decrypt($this->session->ses_id);
		$filter = ["permit_key"=>$permit_key,"reference_number"=>$reference_number];

		$check_exist = $this->Admin_model->count_rows("permit_key",$filter);
		if($check_exist < 1){
			$response["message"] = "error";
		}
		echo json_encode($response);
	}
}