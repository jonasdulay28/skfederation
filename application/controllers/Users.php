<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model('Admin_model');
	}

	public function index()
	{
		$this->load->view('templates/admin_login_template');
	}

	public function login()
	{
		$data['content'] = 'pages/login';
		$this->load->view('templates/authentication_template',$data);
	}

	public function dashboard(){
		$this->load->view('templates/admin_template');
	}


	public function get_users() {
		$data['users'] = $this->Admin_model->fetch('users');
		echo json_encode($data);
	}



	public function add()
	{
		$response = ["message"=>"success"];
		$user_data = json_decode(post('user_data'));
		$username =  $this->generate_username2();

		if($user_data->role == "Student"){
			$user_data->username = $username;
		} 

		$this->Admin_model->insert('users',$user_data);
		echo json_encode($response);
	}

	public function get_user(){
		$username = clean_data(rawurldecode(get('q')));
		$filter = ["username"=>$username];
		$data["user"] = $this->Admin_model->fetch_data('users',$filter);
		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$user_data = json_decode(post('user_data'));
		$username = clean_data(post('username'));
		$filter = ["username"=>$username]; 
		$this->Admin_model->update('users',$user_data,$filter);
		echo json_encode($response);
	}

	public function delete()
	{
		$username = clean_data(post('username'));
		$filter = ["username"=>$username]; 
		$this->Admin_model->delete('users',$filter);
		echo json_encode($response);
	}

	public function changeStatus(){
		$username = clean_data(post('username'));
		$status = clean_data(post('status'));
		$status = !$status;
		$data = ["status"=>$status];
		$filter = ["username"=>$username]; 
		$this->Admin_model->update('users',$data,$filter);
		echo json_encode($response);
	}

	public function generate_username2(){
		$username =  "";
		{
			//regenerate id if exists
			$username = date('Y').mt_rand(10000,99999);
			$filter=array("username"=>$username);
			$check_exist= $this->Admin_model->check_exist("users",$filter);
		}while($check_exist > 0);
		
		return $username;
	}

	public function generate_username(){
		$username =  "";
		{
			//regenerate id if exists
			$username = date('Y').mt_rand(10000,99999);
			$filter=array("username"=>$username);
			$check_exist= $this->Admin_model->check_exist("users",$filter);
		}while($check_exist > 0);
		
		echo json_encode(array("username"=>$username));
	}

	
}