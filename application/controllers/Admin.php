<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model('Admin_model');
	}

	public function index()
	{
		if(!isset($this->session->ses_id)){
			$this->load->view('templates/admin_login_template');
		}else {
			$role = $this->session->ses_role;
			redirect(base_url('Admin/dashboard'));
		}
		
	}

	public function login()
	{
			$response = ["message"=>"error"];
			$filter = [];
			foreach ($_POST as $key => $value) {
				$filter[$key] = clean_data($value);
			}

			//check if activated or not
			$filter["status"] = 1;
			//check if user exist
			$result = $this->Admin_model->fetch_tag_row("*","users",$filter);
			if($result) {
				$this->session->set_userdata('ses_id',encrypt($filter['username']));
				$this->session->set_userdata('ses_role',encrypt($result->role));
				$response["message"] = "success";
			}
			echo json_encode($response);
	}

	public function dashboard(){
		if(isset($this->session->ses_id) && isset($this->session->ses_role)){
				$this->load->view('templates/admin_template');	
		}else{
			redirect(base_url('admin'));
		}
		
	}

	public function logout() {
		session_destroy();
		$res["url"] = base_url('admin');
		echo json_encode($res);
	}


	public function register()
	{
		$data['content'] = 'pages/register';
		$this->load->view('templates/authentication_template',$data);
	}

	public function add()
	{

	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	
}