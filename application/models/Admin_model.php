<?php
class Admin_model extends MY_Model{

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Manila');

	}

	public function get_feedbacks(){
		$query = $this->db->select("a.*,b.first_name as first_name,b.last_name as last_name,b.middle_name as middle_name,b.email as email")->from("feedback a")
		->join('users b', 'b.reference_number = a.reference_number')->get();
		return $query->result();
	}
}